%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%if 0%{?fedora}
%global with_python3 1
%{!?python3_version: %global python3_version %(%{__python3} -c "import sys; sys.stdout.write(sys.version[:3])")}
%endif

%define srcname pilkit

Name:           python-pilkit
Version:        1.1.12
Release:        2%{dist}
Summary:        Utilities and processors for the Python Imaging Libary
Group:          Development/Libraries
License:        BSD
URL:            http://github.com/matthewwithanm/pilkit/
Source0:        http://pypi.python.org/packages/source/p/%{srcname}/%{srcname}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  python-devel >= 2.6
BuildRequires:  python-mock
BuildRequires:  python-nose
BuildRequires:  python-nose-progressive
BuildRequires:  python-pillow
%if 0%{?with_python3}
BuildRequires:  python3-devel >= 3.1
BuildRequires:  python3-mock
BuildRequires:  python3-nose
BuildRequires:  python3-nose-progressive
BuildRequires:  python3-pillow
%endif # with_python3

%description
PILKit is a collection of utilities for working with PIL (the Python Imaging
Library).

%if 0%{?with_python3}
%package -n python3-pilkit
Summary:        Utilities and processors for the Python Imaging Libary.
Group:          Development/Languages
Requires:       python3 >= 3.1

%description -n python3-pilkit
PILKit is a collection of utilities for working with PIL (the Python Imaging
Library).
%endif # with_python3

%prep
%setup -q -n %{srcname}-%{version}

# remove shebangs
find pilkit -type f -name '*.py' \
  -exec sed -i -e '/^#!/{1D}' {} \;

# fix line-ending
find docs -type f \
  -exec sed -i 's/\r//' {} \;

# loosen nose deps
sed -i setup.py \
  -e 's/nose==1.2.1/nose/' \
  -e 's/nose-progressive==1.3/nose-progressive/'

%if 0%{?with_python3}
cp -a . %{py3dir}
%endif # with_python3

%build
%{__python} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py build
popd
%endif # with_python3

%install
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install -O1 --skip-build --root %{buildroot}
popd
%endif # with_python3

%check
%{__python2} setup.py test

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py test
popd
%endif # with_python3

%files
%doc docs/*
%{python_sitelib}/*

%if 0%{?with_python3}
%files -n python3-pilkit
%doc docs/*
%{python3_sitelib}/*
%endif # with_python3

%changelog
* Fri Jan 02 2015 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.1.12-2
- Fix summary-ended-with-dot rpmlint warning

* Mon Oct 27 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.1.12-1
- New package.
